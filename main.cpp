#include <iostream>

#include <SDL2/SDL_mixer.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL.h>

#include <gert_FontWrapper.h>
#include <gert_Wrappers.h>
#include <GameManager.h>
#include <TexHolder.h>
#include <cstring>
#include <stdlib.h>
#include <../version.h>

#include <gert_Camera.h>

Camera gCamera;
GameManager* gGameManager;
SDL_Window* gWindow;

MultiFrame_Wrap* gTextures;
Font_Wrapper* gFont;
double gFrameTime;

void loadAssets( void )
{
    using namespace TexHolder;
    gTextures = new MultiFrame_Wrap[ tTOTAL ];

    gFont = new Font_Wrapper( "assets/CamingoCode-Regular.ttf", 18 );
}

void closeAssets( void )
{
    delete[] gTextures;

    if( gFont != nullptr )
    {
        delete gFont;
    }
}

int main( int argc, char* argv[] )
{
    if( SDL_Init( SDL_INIT_EVERYTHING ) < 0 )
    {
        std::cout << SDL_GetError() << std::endl;
        return 1;
    }
    if( IMG_Init( IMG_INIT_JPG | IMG_INIT_PNG ) < 0 )
    {
        std::cout << IMG_GetError() << std::endl;
        return 2;
    }
    if( TTF_Init() < 0 )
    {
        std::cout << TTF_GetError() << std::endl;
        return 3;
    }
    if( Mix_OpenAudio( 44100, MIX_DEFAULT_FORMAT, 2, 1024 ) < 0 )
    {
        std::cout << Mix_GetError() << std::endl;
        return 4;
    }
    SDL_StartTextInput();

    gCamera = { 0, 0, 1280, 720, 1 };

    if( argc > 1 )
    {
        for( uint8_t i = 1; i < argc; i++ )
        {
            if( std::strcmp( argv[ i ], "-lowres" ) == 0 )
            {
                gCamera.w = 800;
                gCamera.h = 600;
            }
            else if( std::strcmp( argv[ i ], "-width" ) == 0 )
            {
                gCamera.w = strtol( argv[ i+1 ], nullptr, 10 );
            }
            else if( std::strcmp( argv[ i ], "-height" ) == 0 )
            {
                gCamera.h = strtol( argv[ i+1 ], nullptr, 10 );
            }
        }
    }
    RESIZE_CAMERA

    std::cout << AutoVersion::STATUS << "\t" << AutoVersion::FULLVERSION_STRING << std::endl;

    gWindow = SDL_CreateWindow( "Plight of Jeffrey", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, gCamera.w, gCamera.h, SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE );
    SDL_Renderer* rendera = SDL_CreateRenderer( gWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC );

    loadAssets();
    gGameManager = new GameManager;

    Uint32 frameTime = 0;
    while( gGameManager->gameState != GameManager::gmsQUITTING )
    {
        gFrameTime = ( SDL_GetTicks() - frameTime ) * 0.001;
        frameTime = SDL_GetTicks();

        gGameManager->update();
        gGameManager->draw();

        #define MAX_FPS ( 1000.0/90 )
        if( SDL_GetTicks() - frameTime < MAX_FPS )
        {
            SDL_Delay( MAX_FPS - ( SDL_GetTicks() - frameTime ) );
        }
    }

    SDL_StopTextInput();
    closeAssets();

    SDL_DestroyRenderer( rendera );
    SDL_DestroyWindow( gWindow );

    SDL_Quit();
    IMG_Quit();
    TTF_Quit();
    //Mix_Quit();

    delete gGameManager;
    return 0;
}
