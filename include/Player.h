#ifndef PLAYER_H
#define PLAYER_H

#include <Actor.h>
#include <string>

union SDL_Event;
class SDL_Point;

extern double gFrameTime;

class Player : public Actor
{
    public:
        Player();
        virtual ~Player();

        void draw( void );
        void update( void );
        bool manage_input( const SDL_Event& event );

        std::string myInput;
        bool talkMode;
    protected:
        float _animationTime;
        SDL_Point* _moveTo;

    private:
};

#endif // PLAYER_H
