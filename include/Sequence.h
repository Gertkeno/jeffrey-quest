#ifndef SEQUENCE_H
#define SEQUENCE_H

namespace seq
{
    enum num: unsigned char
    {
        THE_BEGINNING,
        THE_END,
        NOT_APPLICABLE
    };
}

#endif // SEQUENCE_H
