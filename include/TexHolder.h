#ifndef TEXHOLDER_H
#define TEXHOLDER_H

class MultiFrame_Wrap;
extern MultiFrame_Wrap* gTextures;
class Font_Wrapper;
extern Font_Wrapper* gFont;

#define FONT_HEIGHT gFont->string_height()

namespace TexHolder
{
    enum texNames
    {
        BACK_GROUND,
        tTOTAL
    };
};

#endif // TEXHOLDER_H
