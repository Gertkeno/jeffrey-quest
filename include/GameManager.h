#ifndef GAMEMANAGER_H
#define GAMEMANAGER_H

#include <string>
#include <Sequence.h>

class SDL_Point;
union SDL_Event;
class SDL_Window;
extern SDL_Window* gWindow;

typedef class _Mix_Music Mix_Music;

#ifdef _GERT_DEBUG
class SDL_RWops;
#endif // _GERT_DEBUG

#ifndef gRenderer
#define gRenderer SDL_GetRenderer( gWindow )
#endif // gRenderer

class Player;
class Camera;
extern Camera gCamera;

class SceneData;

#ifndef RESIZE_CAMERA
#define TEXT_GAP 200
#define RESIZE_CAMERA {\
    gCamera.z = gCamera.w / 1280.0;\
    double zTemp = ( gCamera.h - TEXT_GAP ) / 720.0;\
    if( zTemp < gCamera.z ) gCamera.z = zTemp; }
#endif // RESIZE_CAMERA

class GameManager
{
    public:
        GameManager();
        virtual ~GameManager();

        enum gmstates
        {
            gmsPLAYING,
            gmsQUITTING
        };

        gmstates gameState;

        void update( void );
        void draw( void );

        void push_text( std::string txt );

        void load_dialogue( const char* foo );
        void load_scene( const char* foo );
        void load_music( const char* foo );

        void set_sequence( seq::num n );
        seq::num check_sequence( void );

        void save_game( const char* fName );
        bool load_game( const char* fName );

        Player* theJeffrey;
    private:
        ///Sequencing
        seq::num _sequence;
        bool _brokenSequence;

        SDL_Point* _mouse;
        SDL_Event* _event;
        std::string* _textLog;

        ///Scene and Dialogue data
        SceneData* _generalScene;
        SceneData* _generalDialogue;
        SceneData* _activeScene;
        SceneData* _activeDialogue;

        ///Music
        Mix_Music* _song;

        void _action_from_player( void );

        #ifdef _GERT_DEBUG
        SDL_RWops* _badInputsFile;
        #endif // _GERT_DEBUG
};

#endif // GAMEMANAGER_H
