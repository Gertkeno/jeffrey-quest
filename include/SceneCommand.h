#ifndef SCENECOMMAND_H
#define SCENECOMMAND_H

#include <Sequence.h>

class GameManager;
extern GameManager* gGameManager;

namespace scCommand
{

    struct SceneCommand
    {
        SceneCommand();
        virtual ~SceneCommand();

        virtual void start( void );

        SceneCommand* chain;
    };

    struct DialoguePush: public SceneCommand
    {
        DialoguePush( void );
        virtual ~DialoguePush( void );

        virtual void start( void );

        char* text;
    };

    struct SetSequence: public SceneCommand
    {
        SetSequence( void ) { mySeq = seq::NOT_APPLICABLE; }

        void start( void );

        seq::num mySeq;
    };

    struct NeedSequence: public SceneCommand
    {
        NeedSequence( void ) { mySeq = seq::NOT_APPLICABLE; }

        virtual void start( void );

        seq::num mySeq;
    };

    struct ElseLogic: public NeedSequence
    {
        void start( void );
    };

    struct TalkTo: public DialoguePush
    {
        void start( void );
    };

    struct OpenScene: public DialoguePush
    {
        void start( void );
    };

    struct PlayMusic: public DialoguePush
    {
        void start( void );
    };
}

#endif // SCENECOMMAND_H
