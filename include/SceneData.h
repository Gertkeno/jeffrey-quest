#ifndef SCENEDATA_H
#define SCENEDATA_H

#include <string>

namespace scCommand
{
    struct SceneCommand;
}

class SceneData
{
    public:
        SceneData();
        virtual ~SceneData();

        void load( const char* fName );
        void close( void );
        bool proc_command( const char* sinput );

        bool get_active( void );

        void show_commands( void );
    protected:
        std::string* _procOn;
        scCommand::SceneCommand** _command;
        uint16_t _totalProc;

    private:
};

#endif // SCENEDATA_H
