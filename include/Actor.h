#ifndef ACTOR_H
#define ACTOR_H

struct SDL_Point;

class Actor
{
    public:
        Actor( void );
        virtual ~Actor( void );

        virtual void update( void ) = 0;
        virtual void draw( void ) = 0;
        virtual void start( const SDL_Point& pos );

        SDL_Point get_syncPoint( void );
    protected:
        SDL_Point* _syncPoint;
};

#endif // ACTOR_H
