#ifndef CAMERA_H
#define CAMERA_H

#include <SDL2/SDL_rect.h>

struct Camera
{
	int x, y, w, h;
	double z;

	SDL_Rect ct_rect( void ) { return { x, y, int(w/z), int(h/z) }; };
};

#endif //CAMERA_H
