#include "gert_FontWrapper.h"

#include <SDL2/SDL_shape.h>
#include <SDL2/SDL_ttf.h>
#include <gert_Camera.h>

#include <iostream>

#define STORAGE_FONT 45

//FONT WRAPPER
Font_Wrapper::Font_Wrapper( void )
{
    _myFont = nullptr;
    _lastTexture = new SDL_Texture*[ STORAGE_FONT ];
    _textSize = nullptr;
    _activeStr = 0;
}

Font_Wrapper::~Font_Wrapper( void )
{
    for( uint8_t i = 0; i < STORAGE_FONT; i++ )
    {
        SDL_DestroyTexture( _lastTexture[ i ] );
    }
    delete[] _lastTexture;
    delete[] _lastString;
    TTF_CloseFont( _myFont );
    delete _textSize;
}

Font_Wrapper::Font_Wrapper( std::string fontPath, int fontSize )
{
    _activeStr = 0;
    _textSize = new int( fontSize );
    _myFont = TTF_OpenFont( fontPath.c_str(), fontSize );
    if( _myFont == NULL )
    {
        std::cout << "Couldn't load font \n>" << TTF_GetError() << std::endl;
    }
    TTF_SizeText( _myFont, "Test", NULL, &t_height );

    _lastTexture = new SDL_Texture*[ STORAGE_FONT ];
    for( uint8_t i = 0; i < STORAGE_FONT; i++ )
    {
        _lastTexture[ i ] = nullptr;
    }
    _lastString = new std::string[ STORAGE_FONT ];
}

bool Font_Wrapper::render( SDL_Rect area, std::string text, SDL_Color color, bool useCamera, bool strech )
{
    if( !strech && text.length() > 0 ) //text wrapping past edges
    {
        int wStrech = 0;// = text.length() * (*_textSize)/FONT_MULTI;
        TTF_SizeText( _myFont, text.c_str(), &wStrech, NULL );
        if( wStrech < area.w )
        {
            area.w = wStrech;
        }
        else
        {
            int wStrLimit = 0;
            for( int tempWidth = 0; tempWidth < area.w; TTF_SizeText( _myFont, text.substr( 0, wStrLimit ).c_str(), &tempWidth, NULL ) )
            {
                wStrLimit += 2;
            }
            wStrLimit -= 1;

            if( wStrLimit < int( text.length() ) && wStrLimit > 0 )
            {
                std::string slicedText = text.substr( wStrLimit );

                SDL_Rect subArea = { area.x, area.y + *_textSize, area.w, area.h - *_textSize };
                text = text.substr( 0, wStrLimit );
                render( subArea, slicedText, color, useCamera, strech );
            }
        }

        area.h = *_textSize;
    }// STRECH TEXT WRAPPING

    ///fit to camera
    if( useCamera )
    {
        area.x -= gCamera.x;
        area.y -= gCamera.y;

        area.x *= gCamera.z;
        area.y *= gCamera.z;
        area.w *= gCamera.z;
        area.h *= gCamera.z;
    }

    ///check for existing textures
    int existing = -1;
    for( uint8_t i = 0; i < STORAGE_FONT; i++ )
    {
        if( _lastString[ i ] == text )
        {
            existing = i;
            break;
        }
    }

    ///actual rendering
    if( existing >= 0 && _lastTexture[ existing ] != nullptr );
    else
    {
        SDL_DestroyTexture( _lastTexture[ _activeStr ] );
        _lastTexture[ _activeStr ] = nullptr;
        _lastString[ _activeStr ] = text;
        SDL_Surface *tempMsg = TTF_RenderText_Blended( _myFont, text.c_str(), { 255, 255, 255, 255 } );
        if( tempMsg == NULL )
        {
            //std::cout << "Couldn't create text \n>" << TTF_GetError() << std::endl;
        }
        else
        {
            _lastTexture[ _activeStr ] = SDL_CreateTextureFromSurface( gRenderer, tempMsg );
            existing = _activeStr;
            _activeStr += 1;
            if( _activeStr >= STORAGE_FONT )
            {
                _activeStr = 0;
            }
        }
        SDL_FreeSurface( tempMsg );
    } // NOT THE LAST STRING
    if( text != "\0" && area.x < gCamera.w && area.y < gCamera.h )
    {
        SDL_SetTextureAlphaMod( _lastTexture[ existing ], color.a );
        SDL_SetTextureColorMod( _lastTexture[ existing ], color.r, color.g, color.b );
        if( !strech ) TTF_SizeText( _myFont, _lastString[ existing ].c_str(), &area.w, &area.h );
        SDL_RenderCopy( gRenderer, _lastTexture[ existing ], NULL, &area );
    }
    return true;
}

int Font_Wrapper::string_width( std::string ss )
{
    int w;
    TTF_SizeText( _myFont, ss.c_str(), &w, NULL );
    return w + 1;
}
