#include "Actor.h"
#include <SDL2/SDL.h>

Actor::Actor()
{
    //ctor
    _syncPoint = new SDL_Point( { -1, -1 } );
}

Actor::~Actor()
{
    //dtor
    delete _syncPoint;
}

SDL_Point Actor::get_syncPoint( void )
{
    return *_syncPoint;
}

void Actor::start( const SDL_Point& pos )
{
    *_syncPoint = pos;
}
