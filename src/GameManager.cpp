#include "GameManager.h"
#include <SDL2/SDL_mixer.h>
#include <SDL2/SDL.h>

#include <gert_SoundWrapper.h>
#include <gert_FontWrapper.h>
#include <gert_CameraMath.h>
#include <gert_Wrappers.h>
#include <gert_Camera.h>
#include <TexHolder.h>
#include <SceneData.h>
#include <Player.h>
#include <cstring>

#ifdef _GERT_DEBUG
#include <iostream>
#endif // _GERT_DEBUG

#define MAX_CHAT_LOG 40

GameManager::GameManager()
{
    //ctor
    gameState = gmsPLAYING;
    _brokenSequence = false;
    _song = nullptr;

    _event = new SDL_Event;
    theJeffrey = new Player;
    _textLog = new std::string[ MAX_CHAT_LOG ];
    _sequence = seq::THE_BEGINNING;

    _generalScene = new SceneData;
    _generalScene->load( "assets/scenery/general.txt" );
    _activeScene = new SceneData;

    _generalDialogue = new SceneData;
    _generalDialogue->load( "assets/dialogue/general.txt" );
    _activeDialogue = new SceneData;

    _mouse = new SDL_Point;

    #ifdef _GERT_DEBUG
    _badInputsFile = SDL_RWFromFile( "bad_inupts.txt", "a" );
    if( _badInputsFile == NULL )
    {
        gameState = gmsQUITTING;
    }
    #endif // _GERT_DEBUG
}

GameManager::~GameManager()
{
    //dtor
    delete _event;
    delete theJeffrey;
    delete _textLog;

    delete _activeScene;
    delete _generalScene;

    delete _activeDialogue;
    delete _generalDialogue;

    delete _mouse;

    #ifdef _GERT_DEBUG
    if( _badInputsFile != nullptr )
    {
        SDL_RWclose( _badInputsFile );
    }
    #endif // _GERT_DEBUG
}

void GameManager::update( void )
{
    while( SDL_PollEvent( _event ) )
    {
        switch( gameState ) //GAMESATE ACTIONS
        {
        case gmsPLAYING:
            if( theJeffrey->manage_input( *_event ) )
            {
                _action_from_player();
            }
            break;
        case gmsQUITTING:
            break;
        }

        switch( _event->type )//GLOBAL ACTIONS
        {
        case SDL_MOUSEMOTION:
            SDL_GetMouseState( &_mouse->x, &_mouse->y );
            break;
        case SDL_WINDOWEVENT:
            if( _event->window.event == SDL_WINDOWEVENT_RESIZED )
            {
                SDL_GetWindowSize( gWindow, &gCamera.w, &gCamera.h );
                RESIZE_CAMERA
            }
            break;
        case SDL_QUIT:
        	gameState = gmsQUITTING;
        	break;//SDL_QUIT
        case SDL_KEYDOWN:
            switch( _event->key.keysym.sym )
            {
            case SDLK_F4:
                gameState = gmsQUITTING;
                break;
            }
            break;
        }//SDL_KEYDOWN
    }//SDL_PollEvent( _event )

    switch( gameState )//GAME STATE UPDATES
    {
    case gmsPLAYING:
        if( !_activeScene->get_active() )
        {
            _activeScene->load( "assets/scenery/loader.txt" );
        }
        theJeffrey->update();
        break;
    case gmsQUITTING:
        break;
    }

    //GLOBAL UPDATES
}

void GameManager::draw( void )
{
    SDL_SetRenderDrawColor( gRenderer, 255, 255, 255, 255 );
    SDL_RenderClear( gRenderer );

    if( gameState == gmsPLAYING )
    {
        {
            SDL_Rect place = rectAlign::middle( { 0, 0, 1280, 720 }, gCamera.ct_rect() );
            gTextures[ TexHolder::BACK_GROUND ].render( place, color::WHITE );
        }

        theJeffrey->draw();

        for( uint8_t i = 0; i < MAX_CHAT_LOG; i++ )
        {
            SDL_Rect tPlace = { 0, gCamera.h - FONT_HEIGHT*(i + 2), gFont->string_width( _textLog[ i ] ), FONT_HEIGHT };
            gFont->render( tPlace, _textLog[ i ], color::BLACK, false );
        }

        SDL_Rect textPlace = { 0, gCamera.h - FONT_HEIGHT, gFont->string_width( theJeffrey->myInput + '_' ), FONT_HEIGHT };
        gFont->render( textPlace, theJeffrey->myInput + '_', color::BLUE, false );
    }

    SDL_RenderPresent( gRenderer );
}

///macro for bad inputs, helps make better commands
#ifdef _GERT_DEBUG
#define BAD_COMMAND\
    if( _badInputsFile != nullptr ) {\
        SDL_RWwrite( _badInputsFile, command, sizeof( command ) - 1, 1 );\
        char nl = '\n';\
        SDL_RWwrite( _badInputsFile, &nl, sizeof( char ), 1 ); }
#endif // _GERT_DEBUG
void GameManager::_action_from_player( void )
{
    ///Convert input to lowercase
    char command[ theJeffrey->myInput.size() + 1 ];
    for( uint16_t i = 0; i < theJeffrey->myInput.size(); i++ )
    {
        command[ i ] = tolower( theJeffrey->myInput.c_str()[ i ] );
    }
    command[ theJeffrey->myInput.size() ] = '\0';

    #define CHECK( x ) (std::strcmp( command, x ) == 0)
    if( !CHECK( "on-load" ) )
    {
        if( theJeffrey->talkMode )
        {
            if( !_activeDialogue->get_active() || !_activeDialogue->proc_command( command ) )
            {
                if( !_generalDialogue->proc_command( command ) )
                {
                    ///oddly custom dialogue
                    if( CHECK( "give the secrets" ) )
                    {
                        _activeDialogue->show_commands();
                    }
                    #ifdef _GERT_DEBUG
                    else
                    {
                        BAD_COMMAND
                    }
                    #endif // _GERT_DEBUG
                }
            }
        }
        else//not talk mode
        {
            if( !_activeScene->get_active() || !_activeScene->proc_command( command ) )
            {
                if( !_generalScene->proc_command( command ) )
                {

                    bool save = true;
                    bool load = true;
                    for( uint16_t i = 0; command[ i ] != '\0' && i < 5; i++ )
                    {
                        if( command[ i ] != "save"[ i ] )
                        {
                            save = false;
                        }
                        if( command[ i ] != "load"[ i ] )
                        {
                            load = false;
                        }
                    }

                    if( save )
                    {
                        std::string foo = "Saved game to ";
                        foo += &command[ 5 ];
                        push_text( foo );
                        save_game( &command[ 5 ] );
                    }
                    else if( load )
                    {
                        std::string foo = "Loading save: ";
                        foo += &command[ 5 ];
                        push_text( foo );
                        load_game( &command[ 5 ] );
                    }

                    ///oddly custom commands
                    if( CHECK( "exit game" ) )
                    {
                        gameState = gmsQUITTING;
                    }
                    else if( CHECK( "give the secrets" ) )
                    {
                        _activeScene->show_commands();
                    }
                    #ifdef _GERT_DEBUG
                    else
                    {
                        BAD_COMMAND
                    }
                    #endif // _GERT_DEBUG
                }
            }
        }
    }// !CHECK( "on-load" )

    theJeffrey->myInput.clear();
}

void GameManager::push_text( std::string text )
{
    for( uint8_t i = MAX_CHAT_LOG - 1; i > 0; i-- )
    {
        _textLog[ i ] = _textLog[ i - 1 ];
    }
    _textLog[ 0 ] = text;
}

#ifdef _GERT_DEBUG
#define BAD_CONTEXT\
    if( _badInputsFile != nullptr ){\
        uint8_t tfooSize = 0;\
        while( foo[ tfooSize ] != '\0' ) tfooSize += 1;\
        SDL_RWwrite( _badInputsFile, foo, tfooSize, 1 );\
        char nl = '\n';\
        SDL_RWwrite( _badInputsFile, &nl, sizeof( char ), 1 ); }
#endif // _GERT_DEBUG

void GameManager::load_dialogue( const char* foo )
{
    if( _activeDialogue != nullptr )
    {
        #ifdef _GERT_DEBUG
        BAD_CONTEXT
        #endif // _GERT_DEBUG
        if( std::strcmp( foo, "NULL" ) == 0 )
        {
            _activeDialogue->close();
            theJeffrey->talkMode = false;
        }
        else
        {
            _activeDialogue->load( foo );
            theJeffrey->talkMode = true;
        }
    }
}

void GameManager::load_scene( const char* foo )
{
    if( _activeScene != nullptr )
    {
        #ifdef _GERT_DEBUG
        BAD_CONTEXT
        #endif // _GERT_DEBUG
        if( std::strcmp( foo, "NULL" ) == 0 )
        {
            _activeScene->close();
        }
        else
        {
            _activeScene->load( foo );
        }
    }
}

void GameManager::load_music( const char* foo )
{
    if( _song != nullptr )
    {
        Mix_FreeMusic( _song );
        _song = nullptr;
    }

    _song = Mix_LoadMUS( foo );
    if( _song != NULL )
    {
        Mix_PlayMusic( _song, -1 );
    }
    #ifdef _GERT_DEBUG
    else
    {
        std::cout << Mix_GetError() << std::endl;
    }
    #endif // _GERT_DEBUG
}

void GameManager::set_sequence( seq::num n )
{
    if( n > _sequence + 1 )
    {
        _brokenSequence = true;
    }
    _sequence = n;
}

seq::num GameManager::check_sequence( void )
{
    return _sequence;
}

void GameManager::save_game( const char* fName )
{
    SDL_RWops* saveFile = SDL_RWFromFile( fName, "w" );
    if( saveFile != NULL )
    {
        SDL_RWwrite( saveFile, &_sequence, sizeof( seq::num ), 1 );
        SDL_RWclose( saveFile );
    }
}

bool GameManager::load_game( const char* fName )
{
    SDL_RWops* saveFile = SDL_RWFromFile( fName, "r" );
    if( saveFile != NULL )
    {
        SDL_RWread( saveFile, &_sequence, sizeof( seq::num ), 1 );
        SDL_RWclose( saveFile );
        return true;
    }
    else
    {
        return false;
    }
}
