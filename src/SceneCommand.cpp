#include "SceneCommand.h"

#include <GameManager.h>
#include <Player.h>

#ifdef _GERT_DEBUG
#include <iostream>
#endif // _GERT_DEBUG

namespace scCommand
{
    ///BASE STUFF
    SceneCommand::SceneCommand()
    {
        //ctor
        chain = nullptr;
    }

    SceneCommand::~SceneCommand()
    {
        //dtor
        if( chain != nullptr && chain != this && chain->chain != this )
        {
            delete chain;
            chain = nullptr;
        }
    }

    void SceneCommand::start( void )
    {
        if( chain != nullptr && chain != this && chain->chain != this )
        {
            chain->start();
        }
    }

    ///DIALOGUE PUSH
    #define DIALOGUE_CHAR_LIMIT 120
    DialoguePush::DialoguePush( void )
    {
        text = new char[ DIALOGUE_CHAR_LIMIT ];
        for( uint8_t i = 0; i < DIALOGUE_CHAR_LIMIT; i++ )
        {
            text[ i ] = '\0';
        }
    }

    DialoguePush::~DialoguePush( void )
    {
        delete[] text;
    }

    void DialoguePush::start( void )
    {
        text[ DIALOGUE_CHAR_LIMIT - 1 ] = '\0';
        gGameManager->push_text( text );
        SceneCommand::start();
    }

    ///SET SEQUENCE
    void SetSequence::start( void )
    {
        gGameManager->set_sequence( mySeq );
        SceneCommand::start();
    }

    ///NEED ITEM
    void NeedSequence::start( void )
    {
        if( gGameManager->check_sequence() >= mySeq )
        {
            SceneCommand::start();
        }
        else if( chain != nullptr && chain->chain != nullptr )
        {
            chain->chain->start();
        }
    }

    ///ELSE ITEM
    void ElseLogic::start( void )
    {
        if( !gGameManager->check_sequence() >= mySeq )
        {
            SceneCommand::start();
        }
        else if( chain != nullptr && chain->chain != nullptr )
        {
            chain->chain->start();
        }
    }

    ///TALK TO
    void TalkTo::start( void )
    {
        gGameManager->load_dialogue( text );
        //SceneCommand::start(); //read: open scene
    }

    ///OPEN SCENE
    void OpenScene::start( void )
    {
        gGameManager->load_scene( text );
        //SceneCommand::start(); //don't chain after loading, just use on-load for any reason to do this
    }

    ///PLAY MUSIC
    void PlayMusic::start( void )
    {
        gGameManager->load_music( text );
        SceneCommand::start();
    }
}
