#include "SceneData.h"
#include <SDL2/SDL_rwops.h>
#include <SceneCommand.h>

#include <cstring>

#include <gert_Wrappers.h>
#include <GameManager.h>
#include <TexHolder.h>

#ifdef _GERT_DEBUG
#include <iostream>
#endif // _GERT_DEBUG

SceneData::SceneData()
{
    //ctor
    _procOn = nullptr;
    _command = nullptr;
    _totalProc = 0;
}

SceneData::~SceneData()
{
    //dtor
    if( _procOn != nullptr )
    {
        delete[] _procOn;
    }
    if( _command != nullptr )
    {
        delete[] _command;
    }
}

#define GET_CSTRING( ptr, size )\
    uint16_t active = 0;\
    char ptr[ size ];\
    while( SDL_RWread( sdFile, &buffer, sizeof( char ), 1 ) > 0 && ( buffer != ' ' && buffer != ';' && buffer != '}' && buffer != '\n' ) && active < size - 1 )\
    {\
        ptr[ active ] = buffer;\
        ptr[ active+1 ] = '\0';\
        active++;\
    }

void SceneData::load( const char* fName )
{
    SDL_RWops* sdFile = SDL_RWFromFile( fName, "r" );

    #ifdef _GERT_DEBUG
    ///this saves the file name so we can use it later in debugging
    char copyName[ 99 ];
    for( uint8_t i = 0; i < 99; i++ )
    {
        copyName[ i ] = fName[ i ];
        copyName[ i + 1 ] = '\0';
    }
    #endif // _GERT_DEBUG

    close();

    if( sdFile != NULL )
    {
        ///FIND TOTAL
        _totalProc = 0;
        char buffer;
        while( SDL_RWread( sdFile, &buffer, sizeof( char ), 1 ) > 0 )
        {
            if( buffer == '}' )
            {
                _totalProc += 1;
            }
        }

        #ifdef _GERT_DEBUG
        std::cout << copyName << "\ttotalprocs: " << int(_totalProc) << std::endl;
        #endif // _GERT_DEBUG
        ///ALLOCATE
        _procOn = new std::string[ _totalProc ];
        _command = new scCommand::SceneCommand*[ _totalProc ];
        for( uint16_t i = 0; i < _totalProc; i++ )
        {
            _command[ i ] = nullptr;
        }

        SDL_RWseek( sdFile, 0, RW_SEEK_SET );

        ///COMMAND CREATION
        uint16_t active = 0;
        bool commenting = false;
        scCommand::SceneCommand** actCommand = &_command[ active ]; // this mess of pointers allows for dynamic chaining
        while( SDL_RWread( sdFile, &buffer, sizeof( char ), 1 ) > 0 )
        {
            if( !commenting )
            {
                switch( buffer )
                {
                case '[':///START COMMAND
                    while( SDL_RWread( sdFile, &buffer, sizeof( char ), 1 ) > 0 && buffer != ']' )
                    {
                        _procOn[ active ] += buffer;
                    }
                    break;
                case '}':///END COMMAND
                    active += 1;
                    actCommand = &_command[ active ];
                    break;
                case '"':///PUSH TEXT
                    {
                        *actCommand = new scCommand::DialoguePush;
                        uint16_t textInc = 0;
                        while( SDL_RWread( sdFile, &buffer, sizeof( char ), 1 ) > 0 && buffer != '"' )
                        {
                            ((scCommand::DialoguePush*)(*actCommand))->text[ textInc++ ] += buffer;
                        }
                    }
                    break;
                case ';':///CHAIN
                    if( actCommand != nullptr )
                    {
                        actCommand = &(*actCommand)->chain;
                    }
                    break;
                case 's':///SET SEQUENCE
                    {
                        GET_CSTRING( pcheck, 5 )

                        if( std::strcmp( pcheck, "et" ) == 0 )
                        {
                            *actCommand = new scCommand::SetSequence;

                            GET_CSTRING( tSeq, 4 )

                            ((scCommand::SetSequence*)(*actCommand))->mySeq = seq::num( strtol( tSeq, nullptr, 10 ) );
                            SDL_RWseek( sdFile, -2, RW_SEEK_CUR );
                        }
                    }
                    break;
                case 'n':///NEED SEQUENCE
                    {
                        GET_CSTRING( pcheck, 5 )

                        if( std::strcmp( pcheck, "eed" ) == 0 )
                        {
                            *actCommand = new scCommand::NeedSequence;

                            GET_CSTRING( tSeq, 4 )

                            ((scCommand::NeedSequence*)(*actCommand))->mySeq = seq::num( strtol( tSeq, nullptr, 10 ) );
                            SDL_RWseek( sdFile, -2, RW_SEEK_CUR );
                        }
                    }
                    break;
                case 'e':///UNDER SEQUENCE
                    {
                        *actCommand = new scCommand::ElseLogic;
                        SDL_RWseek( sdFile, 4, RW_SEEK_CUR );

                        GET_CSTRING( eSeq, 4 )

                        ((scCommand::ElseLogic*)(*actCommand))->mySeq = seq::num( strtol( eSeq, nullptr, 10 ) );
                        SDL_RWseek( sdFile, -2, RW_SEEK_CUR );
                    }
                    break;
                case 't':///TALK TO
                    {
                        GET_CSTRING( properT, 4 )

                        if( std::strcmp( properT, "alk" ) == 0 )
                        {
                            *actCommand = new scCommand::TalkTo;

                            GET_CSTRING( talkName, 100 )

                            for( uint8_t i = 0; talkName[ i ] != '\0'; i++ )
                            {
                                ((scCommand::TalkTo*)(*actCommand))->text[ i ] = talkName[ i ];
                            }
                        }
                    }
                    break;
                case 'l':///LOAD SCENE
                    {
                        GET_CSTRING( properT, 4 )

                        if( std::strcmp( properT, "oad" ) == 0 )
                        {
                            *actCommand = new scCommand::OpenScene;

                            GET_CSTRING( sceneName, 100 )

                            for( uint8_t i = 0; sceneName[ i ] != '\0'; i++ )
                            {
                                ((scCommand::OpenScene*)(*actCommand))->text[ i ] = sceneName[ i ];
                            }
                        }
                    }
                    break;
                case 'p':///PLAY MUSIC
                    {
                        GET_CSTRING( tCheck, 4 )

                        if( std::strcmp( tCheck, "lay" ) == 0 )
                        {
                            *actCommand = new scCommand::PlayMusic;

                            GET_CSTRING( songName, 100 )

                            for( uint8_t i = 0; songName[ i ] != '\0'; i++ )
                            {
                                ((scCommand::PlayMusic*)(*actCommand))->text[ i ] = songName[ i ];
                            }
                        }
                    }
                    break;
                case 'B':///LOAD BACK GROUND
                    {
                        if( SDL_RWread( sdFile, &buffer, sizeof( char ), 1 ) > 0 && buffer == 'G' )
                        {
                            SDL_RWseek( sdFile, 2, RW_SEEK_CUR );
                            uint16_t textInc = 0;
                            char fName[ 255 ];
                            while( SDL_RWread( sdFile, &buffer, sizeof( char ), 1 ) > 0 && buffer != '"' )
                            {
                                fName[ textInc ] = buffer;
                                fName[ textInc+1 ] = '\0';
                                textInc++;
                            }

                            gTextures[ TexHolder::BACK_GROUND ].load_texture( fName );
                        }
                    }
                    break;
                case '`':///COMMENTS
                    commenting = true;
                    break;
                }
            }
            else
            {
                if( buffer == '`' )
                {
                    commenting = false;
                }
            }
            ///NON-COMMANDS
        }//while

        proc_command( "on-load" );

        SDL_RWclose( sdFile );
    }
    else
    {
        #ifdef _GERT_DEBUG
        std::cout << "Couldn't open scene file " << fName << std::endl;
        #endif // _GERT_DEBUG
    }
}

void SceneData::close( void )
{
    if( _command != nullptr )
    {
        for( uint16_t i = 0; i < _totalProc; i++ )
        {
            if( _command[ i ] != nullptr )
            {
                delete _command[ i ];
            }
        }

        delete[] _command;
        _command = nullptr;
    }
    if( _procOn != nullptr )
    {
        delete[] _procOn;
        _procOn = nullptr;
    }

    _totalProc = 0;
}

bool SceneData::get_active( void )
{
    return ( _totalProc > 0 && _procOn != nullptr && _command != nullptr );
}

bool SceneData::proc_command( const char* sinput )
{
    for( uint16_t i = 0; i < _totalProc; i++ )
    {
        bool truncateDot = false;
        bool badText = false;
        uint16_t tdLetter = 0;
        while( tdLetter < _procOn[ i ].size() || sinput[ tdLetter ] != '\0' )
        {
            if( !(_procOn[ i ].c_str()[ tdLetter ] == sinput[ tdLetter ] ) && _procOn[ i ].c_str()[ tdLetter ] != '.' )
            {
                badText = true;
                break;
            }
            else if( _procOn[ i ].c_str()[ tdLetter ] == '.' )
            {
                truncateDot = true;
                break;
            }

            tdLetter += 1;
        }

        if( truncateDot || !badText )
        {
            _command[ i ]->start();
            return true;
        }
    }
    return false;
}

void SceneData::show_commands( void )
{
    for( uint16_t i = 0; i < _totalProc; i++ )
    {
        gGameManager->push_text( _procOn[ i ] );
    }
}
