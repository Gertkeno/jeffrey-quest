#include "Player.h"

#include <gert_FontWrapper.h>
#include <SDL2/SDL_events.h>
#include <gert_Wrappers.h>
#include <TexHolder.h>

Player::Player()
{
    //ctor
    myInput.clear();
    talkMode = false;
    _moveTo = new SDL_Point;
}

Player::~Player()
{
    //dtor
    delete _moveTo;
}

void Player::update( void )
{
    _animationTime += gFrameTime;
}

void Player::draw( void )
{

}

bool Player::manage_input( const SDL_Event& event )
{
    switch( event.type )
    {
    case SDL_KEYDOWN:
        switch( event.key.keysym.sym )
        {
        case SDLK_RETURN:
            return true;
        case SDLK_BACKSPACE:
            if( myInput.size() > 0 )
            {
                myInput.pop_back();
            }
            break;
        }
        break;
    case SDL_TEXTINPUT:
        myInput += event.text.text;
        break;
    }

    return false;
}
